package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Student {
    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;



}
